package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        Map<T, Integer> countNumber = new HashMap<>();
        
        for (T number : list) {
            countNumber.put(number, countNumber.getOrDefault(number, 0) + 1);
            if (countNumber.get(number) >= 3) {
                return number;
            }
        }
        
        return null;
    }
    
}
